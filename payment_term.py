#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class PaymentTerm(ModelSQL, ModelView):
    _name = 'account.invoice.payment_term'

    sales_description = fields.Text('Sales Description', translate=True,
            loading='lazy', help='This description is used on all sale '
            'reports. On sale reports are no lines printed.')
    purchase_description = fields.Text('Purchase Description', translate=True,
            loading='lazy', help='This description is used on all purchase '
            'reports. On purchase reports are no lines printed.')
    invoice_description = fields.Text('Invoice Description', translate=True,
            loading='lazy', help='This description is used on all invoice '
            'reports. On invoice '
            'reports lines are printed when their state is Open or '
            'Paid. But this description is printed anyway.')
    invoice_lines_show = fields.Boolean('Show Lines on Invoice')

    def default_invoice_lines_show(self):
        return True

PaymentTerm()



