# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Payment Term Descriptions for Sale, '
            'Purchase, Invoice.',
    'name_de_DE': 'Buchhaltung Zahlungsbedingungen eigene Beschreibungen ' \
        'für Einkauf, Verkauf, Faktura.',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
        Allows custom descriptions for reports in sale, purchase
        and invoice.
    ''',
    'description_de_DE': '''
        Erlaubt die eigene Beschreibung von Zahlungszielen
        für Berichte im Einkauf, Verkauf und Faktura.
    ''',
    'depends': [
        'account_invoice'
    ],
    'xml': [
        'payment_term.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}

